using System;
using System.Collections.Generic;
// In order to use [Required]
using System.ComponentModel.DataAnnotations;

namespace r.b.c_backend.Models
    
    
{
    //Creates each Attribute for the table
    public class Admin{
        public int adminID { get; set; }
        public String Username {get;set;}     
        public String Password {get;set;}
    }
}
