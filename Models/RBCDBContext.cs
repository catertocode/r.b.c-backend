using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using r.b.c_backend.Models;

namespace r.b.c_backend.Models
{
    public class RBCDBContext : DbContext
    {
        

        public RBCDBContext()
        {
        }

        public RBCDBContext(DbContextOptions<RBCDBContext> options) : base(options)
        {
        }
        // Everything below creates the tables in the database 
               
               
             public DbSet<Menu> Menu { get; set; }
                public DbSet<Admin> Admin { get; set; }
                public DbSet<Orders> Orders { get; set; }
           protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Menu>().ToTable("Menu");
            modelBuilder.Entity<Admin>().ToTable("Admin");
            modelBuilder.Entity<Orders>().ToTable("Orders");
            
        }
    }
}