using System;
using System.Collections.Generic;
// In order to use [Required]
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MySql.Data.Common;
namespace r.b.c_backend.Models
{
  
//Creates each Attribute for the table
//Creates Menu class
    public class Menu
    {
        public int MenuID {get;set;}
        
        public string MenuType {get;set;}
       
        public string MenuName {get;set;}
       
        public string MenuDescription {get;set;}
       
        public string MenuPrice {get;set;}

        
    }
}