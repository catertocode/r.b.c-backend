using System;
using System.Collections.Generic;
// In order to use [Required]
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MySql.Data.Common;
namespace r.b.c_backend.Models
{

   
     public class Orders{
        public int OrdersID {get; set;}
        public string Name {get;set;}
        public string Address {get;set;}
        public string Email {get;set;}
        public string Phone {get;set;} 
        public string City {get;set;}
        public string State {get;set;}
        public string Zip {get;set;}
        public string Date {get;set;}
        //Entrees
        public string entreeOne {get;set;}
        public string entreeTwo {get;set;}
        public string entreeThree {get;set;}
        public string entreeFour {get;set;}
        //Sides
        public string sideOne {get;set;}
        public string sideTwo {get;set;}
        public string sideThree {get;set;}
        public string sideFour {get;set;}
        public string sideFive {get;set;}
        public string sideSix{get;set;}
    }
}