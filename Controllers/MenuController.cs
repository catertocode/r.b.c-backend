using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using r.b.c_backend.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;

namespace MenuController.Controllers
{
    //Controllers

    [Route("api/[controller]")]
    public class MenuController : Controller
    {
        private readonly RBCDBContext _context;

        public MenuController(RBCDBContext context)
        {
            _context = context;
        //Adding new Menu
            if (_context.Menu.Count() == 0)
            {
                _context.Menu.Add(new Menu { MenuID=0});
                _context.SaveChanges();
            }
        }
        [HttpGet]
                public IEnumerable<Menu> GetAll()
            {
                return _context.Menu.ToList();
            }
        //GetMenu
        [HttpGet("{id}", Name = "GetMenu")]
                public IActionResult GetById(int id)
            {
                var item = _context.Menu.FirstOrDefault(t => t.MenuID == id);
            if (item == null)
            {
                return NotFound();
            }
                return new ObjectResult(item);
            }

        [HttpPost]
                public IActionResult Create([FromBody] Menu item)
{
            if (item == null)
            {
                return BadRequest();
            }

               _context.Menu.Add(item);
                _context.SaveChanges(); 

                return CreatedAtRoute("GetMenu", new { id = item.MenuID }, item);
}
     [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Menu item)
        {
                if (item == null || item.MenuID != id)
        {
                return BadRequest();
        }

                var menu = _context.Menu.FirstOrDefault(t => t.MenuID == id);
                if (menu == null)
        {
                return NotFound();
        }

                menu.MenuID = item.MenuID ;
                menu.MenuType = item.MenuType;
                menu.MenuName = item.MenuName;
                menu.MenuDescription = item.MenuDescription;
                menu.MenuPrice = item.MenuPrice;
                

                _context.Menu.Update(menu);
                _context.SaveChanges();
                return new NoContentResult();
        }
        
        [HttpDelete("{id}")]
                public IActionResult Delete(int id)
        {
                var menu = _context.Menu.FirstOrDefault(t => t.MenuID == id);
                if (menu == null)
        {
                return NotFound();
        }

                _context.Menu.Remove(menu);
                _context.SaveChanges();
                return new NoContentResult();         
    }

}
}

