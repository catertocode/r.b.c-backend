using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using r.b.c_backend.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;

namespace OrdersController.Controllers
{
    

    [Route("api/[controller]")]
    public class OrdersController : Controller
    {
        private readonly RBCDBContext _context;

        public OrdersController(RBCDBContext context)
        {
            _context = context;

            if (_context.Orders.Count() == 0)
            {
                _context.Orders.Add(new Orders { OrdersID=0});
                _context.SaveChanges();
            }
        }
        [HttpGet]
                public IEnumerable<Orders> GetAll()
            {
                return _context.Orders.ToList();
            }

        [HttpGet("{id}", Name = "GetOrder")]
                public IActionResult GetById(int id)
            {
                var item = _context.Orders.FirstOrDefault(t => t.OrdersID == id);
            if (item == null)
            {
                return NotFound();
            }
                return new ObjectResult(item);
            }

        [HttpPost]
                public IActionResult Create([FromBody] Orders item)
{
            if (item == null)
            {
                return BadRequest();
            }

               _context.Orders.Add(item);
                _context.SaveChanges(); 

                return CreatedAtRoute("GetOrder", new { id = item.OrdersID }, item);
}   }
}