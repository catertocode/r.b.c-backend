using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using r.b.c_backend.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;

namespace AdminController.Controllers
{
    

    [Route("api/[controller]")]
    public class AdminController : Controller
    {
        private readonly RBCDBContext _context;
          public AdminController(RBCDBContext context)
        {
            _context = context;

        }

        [HttpPost]
                public ActionResult login([FromBody] Admin AdminModel)
            {
                if (ModelState.IsValid){
                    _context.Admin.Add(AdminModel);
                    _context.SaveChanges();
                }else
                 throw new Exception ();
                    return BadRequest() ;
            }         
    }
}